<?php

namespace App\Http\Controllers;

use App\Exports\CommentExport;
use App\Mail\NewComment;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $com = '';
        $admin = '';
        if (Auth::user()) {
            $admin = Auth::user()->is_admin;
        }
            $comments = Comment::get();
        return view('index', compact('comments', 'admin','com'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = Comment::create(['name' => $request->name, 'comment' => $request->comment]);


        $data = ['id' => $res->id, 'time' => $res->created_at, 'name' => $res->name, 'comment' => $res->comment];
//        $mail = 'goustbook12345@gmail.com';
//        Mail::to($mail)->send(new NewComment($data));
        return $data;


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Comment $comment)
    {
            $com = Comment::find($id);
            $admin = '';
            if (Auth::user()) {
                $admin = Auth::user()->is_admin;
            }

            $comments = Comment::get();
            return view('index', compact('comments', 'admin','com'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update($com, Request $request, Comment $comment)
    {

        $oldcomment = Comment::find($com);
        $upcomment = $request->all();
        $oldcomment['comment'] = $upcomment['comment'];
        $oldcomment->save();

        return redirect()->route('index');

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::find($id)->delete();


    }

    public function export()
    {
        return Excel::download(new CommentExport, 'comment.xlsx');
    }
}
