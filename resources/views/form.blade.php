

    <div class="form-group">
        @empty(!$com)
        <form action="{{route('update',$com )}}" method="POST">
            @csrf
        @endempty
            <label for="name">Оставьте ваш отзыв</label>
            <input type="hidden" name="name" id="name"
                   @auth
                   value="{{Auth::user()->name}}"
                   @endauth

                   @guest
                   value="anon"
                   @endguest
            >


            <textarea type="text" name="comment" cols="60" rows="8" id="comment" class="form-control">@empty(!$com){{$com->comment}}@endempty</textarea>
            <br>
            @empty(!$com)
            <button class="btn btn-info float-right"  >Редактировать</button>
           @endempty
        </form>
        @empty($com)
            <button class="btn btn-danger float-right" id="submit" >Отправить</button>
        @endempty
        </div>


        <script>

            $(function() {
                $('#submit').on('click',function(){
                    var name = $('#name').val();
                    var comment = $('#comment').val();
                    $.ajax({
                        url: '{{ route('store') }}',
                        type: "POST",
                        data: {name:name,comment:comment},
                        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            var result =    '<div class="comment alert alert-info">'+
                                '<p class="float-lg-right"><a href="" class="red" id="red" data-href="/edit/'+data['id']+'">RED</a>|<a href="/destroy/'+data['id']+'" class="delete" data-href="/destroy/'+data['id']+'">X</a></p>'+
                                '<h4>'+data['name']+'</h4>'+
                                '<p><div class="usercom">'+data['comment']+'</div></p>'+
                                '<p><p><small>'+data['time']+'</small></p></div>'
                            $('.coma').append(result);},
                        error: function (msg) {
                            alert('Ошибка');
                        }
                    });
                    $('#comment').val('');
                });

            })


            // $(document).on('click','.red',function(e){
            //     e.preventDefault();
            //     $('#comment').val(' ');
            //     var old = $('div').data('old');
            //     $('#comment').val(old);
            //     var url = $(this).data('href');
            //     //var el = $(this).parents('div.comment');
            //     // var comment =
            //     alert(old);
            //     alert(comment);


            //
            //     $.ajax({
            //         url: url,
            //         type: "post",
            //         data: {comment:comment},
            //         headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            //         },
            //         success: function (php_script_response) {
            //             alert(php_script_response);
            //             //el.detach();
            //         },
            //         error: function (msg) {
            //             alert('Ошибка');
            //         }
            //
            //     });
            //     $('#comment').val(' ');
            // })


            $(document).on('click','.delete',function(e){
                e.preventDefault();
                var url = $(this).data('href');
                var el = $(this).parents('div.comment');


                $.ajax({
                    url: url,
                    type: "post",
                    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        alert('Запись удалена');
                        el.detach();
                    },
                    error: function (msg) {
                        alert('Ошибка');
                    }

                });

            })

        </script>
{{-- </form>--}}
