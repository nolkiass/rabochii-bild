@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="coma ">

        @foreach($comments as $comment)
    <div class="comment alert alert-info">
        @if(!empty($admin))
            <p class="float-lg-right"><a class="red" id="red" href="{{route('edit', $comment->id)}}">RED</a>|<a class="delete" id="delete" href="" data-href="{{route('clean', $comment->id)}}">X</a></p>
        @endif        <h4>{{$comment->name}}</h4>
        <div class="old" data-old="{{$comment->comment}}"><p>{{$comment->comment}}</p></div>
        <p><p><small>{{$comment->created_at}}</small></p>
    </div>

@endforeach
    </div>
 @include('form')
    </div>





@endsection

