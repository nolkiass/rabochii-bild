<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">© 2017-2020</small>
        </div>
        <div class="col-6 col-md">
            <h5>Информация</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Ссылка </a></li>
                <li><a class="text-muted" href="#">Ссылка </a></li>
                <li><a class="text-muted" href="#">Ссылка </a></li>
                <li><a class="text-muted" href="#">Ссылка </a></li>
                <li><a class="text-muted" href="#">Ссылка </a></li>
                <li><a class="text-muted" href="#">Последняя ссылка</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>Полезна информация</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Ссылка</a></li>
                <li><a class="text-muted" href="#">Ссылка еще</a></li>
                <li><a class="text-muted" href="#">Ссылка и еще</a></li>
                <li><a class="text-muted" href="#">Ссылка другая</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>Еще полезнее</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Ссылка</a></li>
                <li><a class="text-muted" href="#">Ссылка</a></li>
                <li><a class="text-muted" href="#">Ссылка</a></li>
                <li><a class="text-muted" href="#">Ссылка</a></li>
            </ul>
        </div>
    </div>
</footer>


