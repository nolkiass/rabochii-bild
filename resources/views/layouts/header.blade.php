<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h2 class="my-0 mr-md-auto font-weight-normal"><a href="{{route('index')}}">Гостивая книга</a></h2>
    <nav class="my-2 my-md-0 mr-md-3">







        @guest()
        <a class="p-2 text-dark" href="{{route('register')}}">Регистрацияя</a>

    </nav>
    <a class="btn btn-outline-primary" href="{{route('login')}}">Войти</a>
    @endguest
    @auth()
        <div class="continer"><h3 class="float-lg-left">Здарвствуйте {{ Auth::user()->name }}</h3></div>
        <a class="btn btn-outline-primary" href="{{route('get-logout')}}">Выйти</a>
        @if(!empty($admin))
            <a class="btn btn-outline-primary" href="{{route('export')}}">Выгрузка в Excel</a>
        @endif

    @endauth

</div>
