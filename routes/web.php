<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/','CommentController', ['only' => ['index','edit', 'store', 'show','update', 'destroy']]);
Route::post('/destroy/{id}','CommentController@destroy')->name('clean');
Route::get('/edit/{id}','CommentController@edit')->name('edit');
Route::post('/update/{id}','CommentController@update')->name('update');

Route::get('comment/export/', 'CommentController@export')->name('export');

Route::get( '/send', 'MailController@send')->name('mail');

Auth::routes();
Route::get('/logout','Auth\LoginController@logout')->name('get-logout');




